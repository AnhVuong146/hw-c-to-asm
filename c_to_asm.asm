sum2:                             ;this line is a label similar to a function called sum2 
        add     r0, r1, r0        ;Adds both a and b into the same register(r0)
        bx      lr                ;This instruction returns control to the calling function by jumping to the address contained in the link register to end the function.
print_the_value:                  ;this line is a label similar to a function called print_the_value 
        mov     r1, r0            ;move the int parameter value into the register r0 and then that is moved into r1
        ldr     r0, .LCPI1_0      ;we then load r0 which is value and we print it
        b       printf            ;call print function to print an integer value
.LCPI1_0:
        .long   .L.str
entry_point:                    ;this line is a label similar to a function called entry_point 
        push    {r4, lr}        ;saves the value of the r4 register into lr incase some important data is stored in that register already
        bl      rand            ;call the random function 
        mov     r4, r0          ;value of random is stored in r0 and we move it to the r4 register
        bl      rand            ;call random again for the int value b
        mov     r1, r4          ;moves the value of rand into r4 and then we move that into r1
        bl      sum2            ;call sum but we use the value stored in regiser r1 as the sum
        pop     {r4, lr}        ;pop the value that was stored in r4 register
        b       print_the_value ;call print function specifically from the print_the_value label/method for more precise integer value
.L.str:
        .asciz  "%d"