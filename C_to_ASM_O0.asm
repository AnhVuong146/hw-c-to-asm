sum2:                               ;this line is a label similar to a function called sum2 
        sub     sp, sp, #8          ;This instruction subtracts 8 bytes from the stack pointer register to reserve 8 bytes of space on the stack for local variables.
        str     r0, [sp, #4]        ;This instruction stores the value of register r0 on the stack at an offset of 4 bytes from the current value of the stack pointer to save the value so that r0 could be use later.
        str     r1, [sp]            ;This instruction stores the value of register r1 on the stack at the current value of the stack pointer to save the value so that r1 could be use later.
        ldr     r0, [sp, #4]        ;This instruction loads the value of the first argument which was previously saved on the stack, into register r0.
        ldr     r1, [sp]            ;This instruction loads the value of the second argument which was previously saved on the stack, into register r1.
        add     r0, r0, r1          ;This instruction adds the values in r0 and r1 together and stores the result back in r0 in order to compute the sum of the two arguments.
        add     sp, sp, #8          ;This instruction adds 8 bytes back to the stack pointer to free the space that was previously reserved for local variables.
        bx      lr                  ;This instruction returns control to the calling function by jumping to the address contained in the link register to end the function.
print_the_value:                    ;this line is a label similar to a function called print_the_value
        push    {r11, lr}           ;This instruction pushes the values of registers r11 and lr onto the stack to save the values of these registers.
        mov     r11, sp             ;this instruction move sp into r11 to save the current value of the stack pointer.
        sub     sp, sp, #8          ;This instruction subtracts 8 bytes from the stack pointer register to reserve 8 bytes of space on the stack for local variables.
        str     r0, [sp, #4]        ;This instruction stores the value of register r0 on the stack at an offset of 4 bytes from the current value of the stack pointer in order to save and use later on.
        ldr     r1, [sp, #4]        ;This instruction loads the value of the first argument which was previously saved on the stack into register r1.
        ldr     r0, .LCPI1_0        ;This instruction loads the address of a string into register r0 which will later be defined.
        bl      printf              ;This instruction calls the printf function which passes the format string in r0 and the value to be printed in r1 as arguments.
        mov     sp, r11             ;This instruction moves the value of r11 back into the stack pointer register to restore the stack pointer to its previous value.
        pop     {r11, lr}           ;This instruction pops the values of r11 and lr off the stack and restoring their previous values. These registers were pushed onto the stack at the beginning of the function.
        bx      lr                  ;This instruction returns control to the calling function by jumping to the address contained in the link register to end the function.
entry_point:					;Label for entry_point
        push    {r11, lr}			;Push value of registers 'r11' and 'lr' on the stack
        mov     r11, sp				;This instruction moves the value of stack pointer 'sp' into 'rll' register
        sub     sp, sp, #16			;Subtract 16 from stack pointer 'sp' to allocate 16 bytes of space on the stack
        bl      rand				;Branch 'rand' function to return a random number to register 'r0'
        str     r0, [sp, #8]		        ;Store value of register 'r0' onto the stack, 8 bytes from 'sp'
        bl      rand				;Branch 'rand' function to return a random number to register 'r0'
        str     r0, [sp, #4]		        ;Store value of register 'r0' onto the stack, 4 byes from 'sp'
        ldr     r0, [sp, #4]		        ;Load value at an offest of 4 bytes from stack pointer 'sp' into register 'r0'
        ldr     r1, [sp, #8]		        ;Load value at an offset of 8 bytes from stack ponter 'sp' onto register 'r1'
        bl      sum2				;Branch to the 'sum2' function, sum of the two numbers and store into 'r0'
        str     r0, [sp]			;Stores value of 'r0' onto the stack, saving the sum
        ldr     r0, [sp]			;Load the value at 'sp' onto register 'r0'
        bl      print_the_value		        ;Branch to 'print_the_value' function
        ldr     r0, [r11, #-4]		        ;Load the value at an offset of -4 bytes from register 'r11' into 'r0'
        mov     sp, r11				;Restore value of the stack pointer
        pop     {r11, lr}			;Pop value of registers 'r11' and 'lr' from the stack to restore to original values
        bx      lr					;Branch to address stored in 'lr'
