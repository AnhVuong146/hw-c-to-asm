 sum2:                                  ; Define a function named "sum2"
        lea     eax, [rdi + rsi]        ; Compute the sum of the values in RDI and RSI and load the result into EAX
        ret                             ; Return the value in EAX to print out the finalized value of EAX

print_the_value:                        ; Define a function named "print_the_value"
        mov     esi, edi                ; Copy the value of EDI into ESI in order to pass the first parameter to printf
        mov     edi, offset .L.str      ; Load the address of the string ".L.str" into EDI to format string for printf
        xor     eax, eax                ; Set EAX to zero to clear the return value register
        jmp     printf                  ; Jump to printf function performing a print call
entry_point:                            ; @entry_point
        push    rbx                     ;push the rbx register on stack(call function)
        call    rand                    ;call a random number(gets stored in eax) 
        mov     ebx, eax                ;move random number(eax) to ebx register which is our a value
        call    rand                    ;call random again(saved in eax again)  
        mov     edi, eax                ;move the random number to edi register which is our b value
        mov     esi, ebx                ;move ebx(first rand number into esi which is a) 
        call    sum2                    ; sum2 uses edi and esi register and adds them edi(b) + esi(a)
        mov     edi, eax                ;the sum is saved in esi and stored in edi 
        pop     rbx                     ;pop the function off the stack(clean stack)
        jmp     print_the_value         ; TAILCALL
.L.str:
        .asciz  "%d"  
