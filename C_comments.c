#include <stdio.h> //this is a library module to read standard inputs and outputs
#include <stdlib.h> //this is a standard library module that contains function such as program terminations and etc. 

__attribute__((noinline)) //This function attribute suppresses the inlining of a function at the call points of the function.
int sum2(int a, int b) { //a function call named sum2 that initializes two variable to add each other
    return a + b;       //this function will return the initialized two variable to add up, hence, print out the sum.
}

__attribute__((noinline)) //This function attribute suppresses the inlining of a function at the call points of the function.
void print_the_value(int value) { //A function call named print_the_value that initializes a value in order to print it.
    printf("%d", value);        //A print function that will print the initialized value. 
}

int entry_point() {     // This function prints sum of two random numbers
    int a = rand(); // R4 Initialize variable a to a random number
    int b = rand(); // R0 Initialize variable b to a random number
    int result = sum2(b, a); //Call the sum2 function to add a and b and assign to 'result'
    print_the_value(result); //Print the sum of a and b
}
